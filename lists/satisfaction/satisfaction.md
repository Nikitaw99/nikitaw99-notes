# Satisfaction.
Oh yes! The power mode is really satisfiying. I mean, many packages are!
## Atom
* #### Power Mode
Pretty good and satisfiying! Particles everywhere and also: code intensifies.
* #### Unfancy File Icons
Some icons, but they still improve your `Atom` experience.

---
## Multiple Editors
* #### Emmet (`Atom`, `Sublime`)
It allows really good snippets.

##### `h1>h2{Hello World!}+Tab`
### =
```html
<h1>
  <h2>Hello World!</h2>
</h1>
```

---
The list goes on and on! The list will be updated a couple of times in the future.
###### Copyright (c) 2016 Nikita Manahov All Rights Reserved.
