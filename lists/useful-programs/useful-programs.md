# Useful Programs for coding
A small list of programs I use for coding everyday.
* #### Atom
![Atom Screenshot](atom.jpg)
The best text editor you can find, it has most of the [Sublime Text 3](#) features, so you will feel at home. You can download it at [Atom.io](www.atom.io).
* #### Git (Bash)
![Git Bash Screenshot](git-bash.jpg)
Your favorite version control systen - `Git`. Everybody likes it, everybody uses it. Come on, check it out at [git-scm](#).
* #### Chrome
![Chrome Screenshot](#)
The best internet browser you may have ever found. It's hard to explain briefly, so here's a list of features:
* Thousands upon thousands of extensions, themes, apps and even games.
* Very fast and powerful.
<p>I mostly go to [Github.com](www.github.com), [Bitbucket.org](www.bitbucket.org) and [Gitter.im](www.gitter.im) for coding.</p>
* #### Open Broadcaster Software
![OBS Screenshot](obs.jpg)
Mostly used fro streaming and recording. Optional for coding, best for live coding.

---
## Combinations
All the good combinations of programs go here.
* #### Atom & Git (Bash)
![Atom + Git Bash Screenshot](atom&git-bash.jpg)
Atom is pretty good text editor, it even shows what changed and where. Well, hard to explain. If you are good at using `Git`, also use `Atom`.
