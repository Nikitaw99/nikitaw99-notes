// Gitk, it's better than git log.
gitk
// Git log graph, which looks very pretty.
git log --graph --oneline --decorate
// To-do: Add the best git log command I found.
